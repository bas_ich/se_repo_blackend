var db = require('../connect/test_connect')
var moment = require('moment')
var errorMessages = require('../const/error_message')
const con = require('../connect/test_connect')
var _ = require('lodash');

exports.get_order_se = () => {
    return (req, res, next) => {
        db.query('SELECT * FROM user_information WHERE user_id = ?', req.user_id, (err, result) => {
            if (err) throw err
            else {
                db.query('SELECT * FROM order_se WHERE se_name = ? OR se_name = ? ORDER BY id DESC ', [result[0].name, req.user_id], (err, result) => {
                    if (err) throw err
                    else {
                        if (!result) {
                            res.status(200).json({
                                'success': false,
                                'error_message': 'ไม่มีรายการของ ' + name
                            })
                        } else {
                            req.result = result
                            next()
                        }
                    }
                })
            }
        })

    }
}
exports.add_invoice_se = () => {
    return (req, res, next) => {
        console.log('add_invoice_se', req.user_id )
        let object = {
            order_se_invoice_id: 'INV' + req.user_id + moment().utc(7).add('years', 543).format('YYYYMMDDHHMM'),
            order_se_id: req.body.order_se_id,
            order_se_invoice_date: req.body.order_se_invoice_date,
            order_se_invoice_date_send: req.body.order_se_invoice_date_send,
            order_se_invoice_detail: req.body.order_se_invoice_detail
        }

        db.query('INSERT INTO order_se_invoice SET ?', object, (err, result) => {
            if (err) throw err
            else {
                db.query('UPDATE order_se SET order_se_status=1 WHERE order_se_id=?', req.body.order_se_id, (err, result) => {
                    if (err) throw err
                    else {
                        next()
                    }
                })

            }
        })
    }
}

exports.get_detail_order_se = () => {
    return (req, res, next) => {
        let order = null
        db.query('SELECT * FROM order_se_payment RIGHT JOIN order_se_invoice ON  order_se_payment.order_se_id=order_se_invoice.order_se_id RIGHT JOIN order_se ON order_se_invoice.order_se_id = order_se.order_se_id WHERE order_se.order_se_id = ?', req.body.order_id, (err, result) => {
            // db.query('SELECT * FROM order_se LEFT JOIN order_se_payment ON order_se_payment.order_se_id=order_se.order_se_id LEFT JOIN order_se_invoice ON order_se.order_se_id = order_se_invoice.order_se_id WHERE order_se.order_se_id=?', req.body.order_id, (err, result) => {
            if (err) throw err
            else {
                if (!result) {

                    res.status(200).json({
                        'success': false,
                        'error_message': 'ไม่มีรายการของ ' + name
                    })
                } else {
                    db.query('SELECT cost FROM plant_stock WHERE plant_name=?', result[0].plant_name, (err, result_plant) => {
                        if (err) throw err
                        order = {
                            ...result[0],
                            ...result_plant[0]
                        }
                        req.result = order
                        next()
                    })

                }
            }
        })

    }
}

exports.get_linechart_some_se = function () {
    return function (req, res, next) {
        db.query(`SELECT user_information.name,manufacture_information.plant_type,farmer_information.title_name,farmer_information.first_name,farmer_information.last_name FROM ((user_information LEFT JOIN farmer_information ON user_information.user_id = farmer_information.user_id) LEFT JOIN manufacture_information ON farmer_information.farmer_id = manufacture_information.farmer_id) WHERE user_information.user_id = ?`, req.user_id, function (err, result) {
            if (err) throw err;
            // let name_se = []
            // let test_result = []
            // let keep_req = result[0].name
            // console.log(result[0].name, req.user_id)

            // let plant_el = []
            // let month = []

            // result.map((el) => {

            //     if (el.name === keep_req) {


            //         let se_plant
            //         try {
            //             se_plant = JSON.parse(el.plant_type)

            //             se_plant.map((ele) => {

            //                 test_result.push(
            //                     {
            //                         name: keep_req,
            //                         plant: ele.plant,
            //                         eliver_frequency_number: ele.eliver_frequency_number,
            //                         deliver_value: ele.deliver_value,
            //                         value: ele.eliver_frequency_number * ele.deliver_value,
            //                         month: ele.end_plant
            //                     }

            //                 )

            //                 index = plant_el.findIndex((elem) => elem === ele.plant)
            //                 if (index < 0) {
            //                     if (ele.plant !== null) {
            //                         plant_el.push(
            //                             ele.plant
            //                         )
            //                     }
            //                 } else {

            //                 }

            //             })
            //         } catch (err) {

            //         }

            //     }



            // })

            // plant_el.map((el_plant, index) => {

            //     let january = 0
            //     let febuary = 0
            //     let march = 0
            //     let april = 0
            //     let may = 0
            //     let june = 0
            //     let july = 0
            //     let august = 0
            //     let september = 0
            //     let october = 0
            //     let november = 0
            //     let december = 0

            //     result.map((elem_plant) => {

            //         let element_plant
            //         try {
            //             element_plant = JSON.parse(elem_plant.plant_type)
            //         }
            //         catch (err) {

            //         }
            //         // console.log(element_plant)
            //         if (element_plant) {


            //             element_plant.map((elem_plant_obj) => {
            //                 if (el_plant === elem_plant_obj.plant && keep_req === elem_plant.name) {

            //                     if (elem_plant_obj.end_plant == "มกราคม") {
            //                         january = january + (elem_plant_obj.deliver_frequency_number * elem_plant_obj.deliver_value)
            //                     } else if (elem_plant_obj.end_plant == "กุมภาพันธ์") {
            //                         febuary = febuary + (elem_plant_obj.deliver_frequency_number * elem_plant_obj.deliver_value)
            //                     } else if (elem_plant_obj.end_plant == "มีนาคม") {
            //                         march = march + (elem_plant_obj.deliver_frequency_number * elem_plant_obj.deliver_value)
            //                     } else if (elem_plant_obj.end_plant == "เมษายน") {
            //                         april = april + (elem_plant_obj.deliver_frequency_number * elem_plant_obj.deliver_value)
            //                     } else if (elem_plant_obj.end_plant == "พฤษภาคม") {
            //                         may = may + (elem_plant_obj.deliver_frequency_number * elem_plant_obj.deliver_value)
            //                     } else if (elem_plant_obj.end_plant == "มิถุนายน") {
            //                         june = june + (elem_plant_obj.deliver_frequency_number * elem_plant_obj.deliver_value)
            //                     } else if (elem_plant_obj.end_plant == "กรกฎาคม") {
            //                         july = july + (elem_plant_obj.deliver_frequency_number * elem_plant_obj.deliver_value)
            //                     } else if (elem_plant_obj.end_plant == "สิงหาคม") {
            //                         august = august + (elem_plant_obj.deliver_frequency_number * elem_plant_obj.deliver_value)
            //                     } else if (elem_plant_obj.end_plant == "กันยายน") {
            //                         september = september + (elem_plant_obj.deliver_frequency_number * elem_plant_obj.deliver_value)
            //                     } else if (elem_plant_obj.end_plant == "ตุลาคม") {
            //                         october = october + (elem_plant_obj.deliver_frequency_number * elem_plant_obj.deliver_value)
            //                     } else if (elem_plant_obj.end_plant == "พฤศจิกายน") {
            //                         november = november + (elem_plant_obj.deliver_frequency_number * elem_plant_obj.deliver_value)
            //                     } else if (elem_plant_obj.end_plant == "ธันวาคม") {
            //                         december = december + (elem_plant_obj.deliver_frequency_number * elem_plant_obj.deliver_value)
            //                     }

            //                 }


            //             })




            //         }
            //     })

            //     month.push({
            //         name: el_plant,
            //         data: [january, febuary, march, april, may, june, july, august, september, october, november, december]
            //     })
            // })
            // name_se.push(
            //     {
            //         se_name: keep_req,
            //         plant: month
            //     }
            // )

            let plant_name = []

            result.map((element) => {
                element.plant_type = JSON.parse(element.plant_type)
                let plant_type = element.plant_type
                if (plant_type !== null) {
                    plant_type.map((ele_pla) => {


                        index = plant_name.findIndex((elem) => elem === ele_pla.plant)
                        if (index < 0) {
                            if (ele_pla.plant !== null && ele_pla.plant !== undefined) {
                                // console.log(ele_pla.plant)
                                plant_name.push(
                                    ele_pla.plant
                                )
                            }
                        }
                    })
                }
            })
            let plant_se = []
            let month = []

            plant_name.map((ele_plant) => {
                let january = 0,
                    febuary = 0,
                    march = 0,
                    april = 0,
                    may = 0,
                    june = 0,
                    july = 0,
                    august = 0,
                    september = 0,
                    october = 0,
                    november = 0,
                    december = 0
                let jan = [],
                    feb = [],
                    mar = [],
                    apr = [],
                    ma = [],
                    jun = [],
                    jul = [],
                    aug = [],
                    sep = [],
                    oct = [],
                    nov = [],
                    dec = []

                result.map((element) => {
                    let plant_type = element.plant_type
                    if (plant_type !== null) {
                        plant_type.map((ele_pla) => {
                            if (ele_plant === ele_pla.plant) {

                                if (ele_pla.end_plant == "มกราคม") {
                                    january = january + (ele_pla.deliver_frequency_number * ele_pla.deliver_value)
                                    jan.push({
                                        ...ele_pla,
                                        title_name: element.title_name,
                                        first_name: element.first_name,
                                        last_name: element.last_name,
                                    })
                                } else if (ele_pla.end_plant == "กุมภาพันธ์") {
                                    febuary = febuary + (ele_pla.deliver_frequency_number * ele_pla.deliver_value)
                                    feb.push({
                                        ...ele_pla,
                                        title_name: element.title_name,
                                        first_name: element.first_name,
                                        last_name: element.last_name,
                                    })
                                } else if (ele_pla.end_plant == "มีนาคม") {
                                    march = march + (ele_pla.deliver_frequency_number * ele_pla.deliver_value)
                                    mar.push({
                                        ...ele_pla,
                                        title_name: element.title_name,
                                        first_name: element.first_name,
                                        last_name: element.last_name,
                                    })
                                } else if (ele_pla.end_plant == "เมษายน") {
                                    april = april + (ele_pla.deliver_frequency_number * ele_pla.deliver_value)
                                    apr.push({
                                        ...ele_pla,
                                        title_name: element.title_name,
                                        first_name: element.first_name,
                                        last_name: element.last_name,
                                    })
                                } else if (ele_pla.end_plant == "พฤษภาคม") {
                                    may = may + (ele_pla.deliver_frequency_number * ele_pla.deliver_value)
                                    ma.push({
                                        ...ele_pla,
                                        title_name: element.title_name,
                                        first_name: element.first_name,
                                        last_name: element.last_name,
                                    })
                                } else if (ele_pla.end_plant == "มิถุนายน") {
                                    june = june + (ele_pla.deliver_frequency_number * ele_pla.deliver_value)
                                    jun.push({
                                        ...ele_pla,
                                        title_name: element.title_name,
                                        first_name: element.first_name,
                                        last_name: element.last_name,
                                    })
                                } else if (ele_pla.end_plant == "กรกฎาคม") {
                                    july = july + (ele_pla.deliver_frequency_number * ele_pla.deliver_value)
                                    jul.push({
                                        ...ele_pla,
                                        title_name: element.title_name,
                                        first_name: element.first_name,
                                        last_name: element.last_name,
                                    })
                                } else if (ele_pla.end_plant == "สิงหาคม") {
                                    august = august + (ele_pla.deliver_frequency_number * ele_pla.deliver_value)
                                    aug.push({
                                        ...ele_pla,
                                        title_name: element.title_name,
                                        first_name: element.first_name,
                                        last_name: element.last_name,
                                    })
                                } else if (ele_pla.end_plant == "กันยายน") {
                                    september = september + (ele_pla.deliver_frequency_number * ele_pla.deliver_value)
                                    sep.push({
                                        ...ele_pla,
                                        title_name: element.title_name,
                                        first_name: element.first_name,
                                        last_name: element.last_name,
                                    })
                                } else if (ele_pla.end_plant == "ตุลาคม") {
                                    october = october + (ele_pla.deliver_frequency_number * ele_pla.deliver_value)
                                    oct.push({
                                        ...ele_pla,
                                        title_name: element.title_name,
                                        first_name: element.first_name,
                                        last_name: element.last_name,
                                    })
                                } else if (ele_pla.end_plant == "พฤศจิกายน") {
                                    november = november + (ele_pla.deliver_frequency_number * ele_pla.deliver_value)
                                    nov.push({
                                        ...ele_pla,
                                        title_name: element.title_name,
                                        first_name: element.first_name,
                                        last_name: element.last_name,
                                    })
                                } else if (ele_pla.end_plant == "ธันวาคม") {
                                    december = december + (ele_pla.deliver_frequency_number * ele_pla.deliver_value)
                                    dec.push({
                                        ...ele_pla,
                                        title_name: element.title_name,
                                        first_name: element.first_name,
                                        last_name: element.last_name,
                                    })
                                }

                            }
                        })
                    }
                })
                month.push({
                    name: ele_plant,
                    data: [january, febuary, march, april, may, june, july, august, september, october, november, december],
                    detail: [jan, feb, mar, apr, ma, jun, jul, aug, sep, oct, nov, dec]
                })
            })
            plant_se.push({
                se_name: result[0].name,
                plant: month
            })

            req.result = plant_se
            next();


        })
    }

}

exports.get_farmer_se = () => {
    return (req, res, next) => {
        let farmer = []
        let plant = []
        db.query('SELECT * from farmer_information INNER JOIN manufacture_information ON farmer_information.farmer_id = manufacture_information.farmer_id WHERE user_id=?', req.user_id, (err, result) => {
            if (err) throw err
            else {
                result.map((element) => {
                    element.plant_type = JSON.parse(element.plant_type)
                    element.plant_type.map((ele) => {
                        if (ele.year_value_unit === 'ตัน') {
                            ele.year_value = ele.year_value * 1000
                        }
                        if (ele.plant !== null && ele.plant !== '' && ele.plant !== undefined) {
                            if (ele.year_value !== null) {
                                if (((ele.deliver_value * 1) !== 0) || ((ele.year_value * 1) !== 0)) {
                                    farmer.push({
                                        farmer_id: element.farmer_id,
                                        title_name: element.title_name,
                                        first_name: element.first_name,
                                        last_name: element.last_name,
                                        plant: ele.plant,
                                        year_value: ele.year_value * 1,
                                        year_value_unit: ele.year_value_unit,
                                        deliver_value: ele.deliver_value * 1,
                                        product_value: ele.product_value * 1,
                                        growingArea: ele.growingarea * 1,
                                        end_plant: ele.end_plant,
                                        deliver_frequency_number: ele.deliver_frequency_number
                                    })
                                }
                            }


                        }


                    })

                })
                // req.result = result[0]
                req.result = farmer
                next()
            }
        })
    }
}

exports.up_stock_se = () => {
    return (req, res, next) => {
        let plant_name = []
        let name_se = []
        db.query('SELECT user_information.user_id,user_information.name,manufacture_information.plant_type,farmer_information.title_name,farmer_information.first_name,farmer_information.last_name FROM ((user_information LEFT JOIN farmer_information ON user_information.user_id = farmer_information.user_id) LEFT JOIN manufacture_information ON farmer_information.farmer_id = manufacture_information.farmer_id) WHERE user_information.type_user = 3', (err, result) => {
            if (err) throw err
            else {
                result.map((element) => {
                    index = name_se.findIndex((elem) => elem.id === element.user_id)
                    if (index < 0) {
                        name_se.push({
                            id: element.user_id,
                            name: element.name
                        })
                    }
                })
                name_se.map((ele_name) => {
                    result.map((element) => {

                        if (ele_name.name === element.name) {
                            element.plant_type = JSON.parse(element.plant_type)
                            let plant_type = element.plant_type
                            if (plant_type !== null) {
                                plant_type.map((ele_pla) => {


                                    index = plant_name.findIndex((elem) => elem.plant_name === ele_pla.plant)
                                    if (index < 0) {
                                        if (ele_pla.plant !== null && ele_pla.plant !== undefined) {
                                            // console.log(ele_pla.plant)
                                            plant_name.push({
                                                id_se: element.user_id,
                                                name_se: element.name,
                                                plant_name: ele_pla.plant
                                            })
                                        }
                                    }
                                })
                            }
                        }
                    })
                })

                req.result = plant_name
                next()
            }
        })
    }
}

exports.add_order_farmer = () => {
    return (req, res, next) => {
        // console.log(req.body)
        req.body.object.map((element) => {
            let obj = {
                order_farmer_id: 'Mr' + moment().utc(7).add('years', 543).format('YYYYMMDDHHMM') + element.farmer_id,
                order_farmer_title_name: element.title_name,
                order_farmer_name: element.first_name,
                order_farmer_lastname: element.last_name,
                order_farmer_plant: element.plant,
                order_farmer_plant_volume: element.amount,
                order_farmer_plant_cost: req.body.cost,
                order_se_id: req.body.order_se_id,
                order_farmer_status: 1,
                se_id: req.user_id,
                order_farmer_date: moment().utc(7).add('years', 543).format()

            }
            db.query('INSERT INTO order_farmer SET ?', obj, (err) => {
                if (err) throw err
                else {
                    db.query('UPDATE order_se SET order_farmer_status=1 WHERE order_se_id=?', req.body.order_se_id, (err) => {
                        if (err) throw err
                        else {
                            next()
                        }
                    })

                }
            })
        })

    }
}

exports.update_order_se_status = () => {
    return (req, res, next) => {
        // console.log(req.body)
        db.query('UPDATE order_se SET order_se_status=? WHERE order_se_id = ?', [req.body.status, req.body.order_se_id], (err) => {
            if (err) throw err
            else {
                next()
            }
        })
    }
}

exports.get_order_farmer = () => {
    return (req, res, next) => {
        db.query('SELECT * FROM order_farmer WHERE order_se_id = ?', req.body.order_se_id, (err, result) => {
            if (err) throw err
            else {
                req.result = result
                next()
            }
        })
    }
}

exports.get_Certified = () => {
    return (req, res, next) => {
        // console.log(req.user_id)
        let data = []
        db.query(`SELECT  
        DISTINCT farmer_information.farmer_id,
        farmer_information.title_name as title_name,
        farmer_information.first_name as first_name,
        farmer_information.last_name as last_name,
        area_join_information.area_storage as area_storage,
        area_join_information.certified as certified,
        area_join_information.chemical_date as chemical_date,
        manufacture_information.plant_type_best as plant_type_best
        FROM farmer_information
         LEFT JOIN (
            SELECT * FROM area_information WHERE area_id IN (
                SELECT MAX(area_id) FROM area_information GROUP BY farmer_id
            )
            ) as area_join_information
        on farmer_information.farmer_id = area_join_information.farmer_id
        LEFT JOIN manufacture_information ON farmer_information.farmer_id = manufacture_information.farmer_id 
        WHERE farmer_information.user_id=?  `, req.user_id, (err, result) => {
            if (err) throw err
            else {

                result.map((element) => {


                    // console.log(JSON.parse(element.certified))
                    data.push({
                        farmer_id: element.farmer_id,
                        title_name: element.title_name,
                        first_name: element.first_name,
                        last_name: element.last_name,
                        area_storage: element.area_storage,
                        certified: JSON.parse(element.certified) || { "num_certified": null, "certified_array": [], "certified_array_other": null, "number_certified_other": null },
                        chemical_date: element.chemical_date,
                        plant_type_best: JSON.parse(element.plant_type_best)
                    })
                })
                req.result = data
                next()
            }
        })
    }
}

exports.get_year_round_se = () => {
    return (req, res, next) => {
        db.query('SELECT * FROM year_round_planing WHERE se_id=? ORDER BY plan_id DESC', req.user_id, (err, result) => {
            if (err) throw err
            else {
                req.result = result
                next()
            }
        })
    }
}

exports.get_planing_farmer = () => {
    return (req, res, next) => {
        db.query('SELECT * FROM year_round_planing_farmer WHERE neo_firm_id=? ORDER BY planing_farmer_id DESC', req.user_id, (err, result) => {
            if (err) throw err
            else {
                req.result = result
                next()
            }
        })
    }
}

exports.get_planing_se_personal = () => {
    return (req, res, next) => {
        db.query('SELECT * FROM year_round_planing WHERE se_name=?', req.user_id, (err, result) => {
            if (err) throw err
            else {
                req.result = result
                next()
            }
        })
    }
}

exports.add_planing_farmer = () => {
    return (req, res, next) => {
        // console.log(req.body)
        req.body.check_array.map((element) => {
            let obj = {
                neo_firm_id: req.user_id,
                planing_farmer_name: element.check,
                planing_farmer_status: 0,
                planing_farmer_volume: element.amount,
                planing_farmer_plant: req.body.name_plant,
                planing_farmer_date: req.body.date
            }
            db.query('INSERT INTO year_round_planing_farmer SET ?', obj, (err) => {
                if (err) throw err
            })
        })
        next()

    }
}

exports.get_count_farmer = () => {
    return (req, res, next) => {
        db.query('SELECT COUNT(*) as sum_farmer FROM farmer_information LEFT JOIN user_information ON farmer_information.user_id = user_information.user_id WHERE farmer_information.user_id=?', req.user_id, (err, result) => {
            if (err) throw err
            else {
                req.result = result
                next()
            }
        })
    }
}

exports.get_trading_statistics_farmer = () => {
    return (req, res, next) => {
        db.query('SELECT * FROM order_farmer WHERE se_id=?', req.user_id, (err, result) => {
            if (err) throw err
            else {
                req.result = result
                next()
            }
        })
    }
}

exports.get_summary_personal = () => {
    return (req, res, next) => {
        db.query('SELECT * FROM order_se INNER JOIN order_se_payment ON order_se.order_se_id = order_se_payment.order_se_id WHERE se_name=?', req.user_id, (err, result) => {
            if (err) throw err
            else {
                // console.log('g',result)
                req.result = result
                next()
            }
        })
    }
}

exports.get_farmer = () => {
    return (req, res, next) => {
        let farmer = []
        let plant = []
        db.query(`SELECT farmer_information.farmer_id,farmer_information.user_id,farmer_information.title_name,farmer_information.first_name,
        farmer_information.last_name,farmer_information.address,farmer_information.phone_number ,manufacture_information.plant_type
        from farmer_information 
        INNER JOIN manufacture_information ON farmer_information.farmer_id = manufacture_information.farmer_id
        WHERE user_id=?`, req.user_id, (err, result) => {
            if (err) throw err
            else {
                let res_array = []
                let growingarea = null
                // console.log(result)
                result.map((result_element) => {
                    let data = []
                    let plant_type = JSON.parse(result_element.plant_type)
                    plant_type.map((plant_type_element) => {
                        // console.log(plant_type_element)

                        data.push({
                            plant: plant_type_element.plant,
                            growingarea: parseInt(plant_type_element.growingarea),
                            product_value: parseInt(plant_type_element.product_value),
                            year_value: parseInt(plant_type_element.year_value),
                            year_value_unit: plant_type_element.year_value_unit

                        })
                    })

                    res_array.push({
                        farmer_id: result_element.farmer_id,
                        user_id: result_element.user_id,
                        title_name: result_element.title_name,
                        first_name: result_element.first_name,
                        last_name: result_element.last_name,
                        phone_number: result_element.phone_number,
                        address: JSON.parse(result_element.address),
                        plant_type: data
                    })
                })
                // console.log(res_array)
                req.result = res_array
                next()
            }
        })
    }
}

exports.get_farmer_phase = () => {
    return (req, res, next) => {
        let farmer = []
        let plant = []
        db.query(`SELECT * FROM famer_phase 
            LEFT JOIN plant_information 
            ON famer_phase.plant_id = plant_information.plant_id
            WHERE farmer_id = ?`, req.params.id, (err, result) => {
            if (err) throw err
            else {
                let res_array = []
                let final_res_array = []

                result.map((result_element) => {
                    let phase = JSON.parse(result_element.phase).filter((e) => e.id === result_element.phase_id)[0]
                    res_array.push({
                        ...result_element,
                        ...phase
                    })
                })

                res_array.map((res_array_element) => {


                    let index = final_res_array.findIndex((e) => e.plant_id === res_array_element.plant_id)
                    
                    if (index >= 0) {
                        final_res_array[index].phase.push({
                            idfamer_phase: res_array_element.idfamer_phase,
                            phase_id: res_array_element.phase_id,
                            phase_name: res_array_element.phase_name,
                            havest_date: moment(res_array_element.havest_date).format("DD-MM-YYYY"),
                            plant_value: res_array_element.plant_value
                        })
                    } else {
                        final_res_array.push({
                            plant_id: res_array_element.plant_id,
                            plant_name: res_array_element.plant_name,
                            phase: [{
                                idfamer_phase: res_array_element.idfamer_phase,
                                phase_id: res_array_element.phase_id,
                                phase_name: res_array_element.phase_name,
                                havest_date: moment(res_array_element.havest_date).format("DD-MM-YYYY"),
                                plant_value: res_array_element.plant_value
                            }]
                        })
                    }
                    // console.log("index : ", index)
                })
                req.result = final_res_array
                next()
            }
        })
    }
}

//////////////////////////////////////////////////////////////////
exports.set_farmer_phase = () => {

    return (req, res, next) => {
        console.log(req.body)

        req.body.data_phase_farmer.map((element) => {
            // console.log(element)
            element.phase.map((element_phase) => {

                // console.log(element_phase)
                let obj = {
                    idfamer_phase: element_phase.idfamer_phase,
                    plant_id: element.plant_id,
                    farmer_id: element.farmer_id,
                    phase_id: element_phase.phase_id,
                    plant_value: parseInt(element_phase.plant_value),
                    havest_date: moment(element_phase.havest_date).format("YYYY-MM-DD"),
                }
                // console.log(obj)

                if (obj.idfamer_phase >= 0) {
                    if (obj.idfamer_phase >= 0) {
                        db.query('UPDATE famer_phase SET plant_value=?, havest_date=? WHERE idfamer_phase=? ', [obj.plant_value, obj.havest_date, obj.idfamer_phase], (err, result) => {
                            if (err) throw err
                            else {
                                next()
                            }
                        })
                    }
                    else {
                        db.query('INSERT INTO famer_phase SET ?', obj, (err, result) => {
                            if (err) throw err
                            else {
                                next()
                            }
                        })
                    }
                }
                else {
                    db.query('INSERT INTO famer_phase SET ?', obj, (err, result) => {
                        if (err) throw err
                        else {
                            next()
                        }
                    })
                }
            })
        })
    }
}

exports.delete_farmer_phase = () => {

    return (req, res, next) => {
        // console.log(req.body.idfamer_phase)

        db.query('DELETE FROM famer_phase WHERE famer_phase.idfamer_phase=?', req.body.idfamer_phase, (err) => {
            if (err) throw err
            else {
                next()
            }
        })
    }
}

exports.update_farmer_phase = () => {
    
    return (req, res, next) => {
        // console.log(req.body)

        let obj = {
            plant_value: parseInt(req.body.plant_value),
            havest_date: moment(req.body.havest_date, "DD-MM-YYYY").format("YYYY-MM-DD")
        }
        // console.log(obj)

        db.query('UPDATE famer_phase SET plant_value=?, havest_date=? WHERE idfamer_phase=? ', [obj.plant_value, obj.havest_date, req.body.idfamer_phase], (err, result) => {
            if (err) throw err
            else {
                next()
            }
        })
    }
}

exports.get_plant_phase = () => {
    return (req, res, next) => {
        db.query('SELECT * FROM plant_information', (err, result) => {
            if (err) throw err
            else {
                let result_array = []

                result.map((element) => {
                    result_array.push({
                        ...element,
                        phase: JSON.parse(element.phase) || []
                    })
                })
                

                req.result = result_array
                next()
            }
        })
    }
}

exports.get_plant_data = () => {
    return (req, res, next) => {
        db.query('SELECT * FROM plant_stock', (err, result) => {
            if (err) throw err
            else {
                db.query('SELECT * FROM order_trader WHERE order_status<=4', (err, result_order) => {
                    // console.log(result_order)
                    if (err) throw err
                    else {
                        result_order.map((element) => {
                            try {
                                element.detail = JSON.parse(element.detail)
                            } catch (error) {
                                // console.log(error)
                            }
                        })

                        let plant = []
                        result.map((ele_result) => {
                            let amount = 0

                            result_order.map((element) => {
                                let detail = element.detail
                                detail.map((element_detail) => {
                                    if (ele_result.plant_id == element_detail.plant_id) {
                                        // console.log(ele_result.plant_name,element_detail.amount)
                                        amount += element_detail.amount * 1
                                    }
                                })

                            })
                            plant.push({
                                plant_id: ele_result.plant_id,
                                plant_name: ele_result.plant_name,
                                amount_stock: ele_result.amount_stock,
                                amount_want: amount
                            })


                        })
                        req.result = plant
                        next()
                    }
                })

            }
        })
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
exports.get_data_farmer_phase = () => {
    return (req, res, next) => {
        // console.log("req.params.phase_id", req.params.phase_id)
        // console.log("params.id", req.user_id)

        db.query(`SELECT  farmer_information.title_name, farmer_information.first_name, farmer_information.last_name, 
                famer_phase.plant_id, farmer_information.farmer_id, famer_phase.plant_value, 
                famer_phase.havest_date, farmer_information.user_id,
                famer_phase.phase_id, neofirm_plan.plant_value_want
                FROM farmer_information 
                LEFT JOIN (SELECT * FROM neofirm_plan WHERE phase_id =?) neofirm_plan
                ON farmer_information.farmer_id = neofirm_plan.farmer_id 
                LEFT JOIN (SELECT * FROM famer_phase WHERE phase_id = ?) famer_phase
                ON farmer_information.farmer_id = famer_phase.farmer_id 
                WHERE user_id = ?`, [req.params.phase_id, req.params.phase_id, req.user_id], (err, result) => {

            if (err) throw err
            else {
                let farmer_phase = []

                result.map((result_element) => {
                    // console.log(result_element)
                    farmer_phase.push({
                        first_name: result_element.first_name,
                        last_name: result_element.last_name,
                        plant_id: result_element.plant_id != null ? result_element.plant_id : 0,
                        farmer_id: result_element.farmer_id,
                        plant_value: result_element.plant_value != null ? result_element.plant_value : 0,
                        havest_date: result_element.havest_date != null ? moment(result_element.havest_date).format("YYYY-MM-DD") : moment().format("YYYY-MM-DD"),
                        phase_id: req.params.phase_id,
                        plant_value_want: result_element.plant_value_want != null ? result_element.plant_value_want : 0,
                    })
                })
                // console.log(farmer_phase)
                req.result = farmer_phase
                next()
            }
        })
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
exports.set_data_plan = () => {
    return (req, res, next) => {

        db.query('SELECT * FROM neofirm_plan', (err, result) => {

            // console.log(req.body.neofirm_plan)

            req.body.neofirm_plan.map((element_neo) => {
                // console.log(element_neo)
                let obj = {
                    farmer_id: element_neo.farmer_id,
                    plant_id: element_neo.plant_id,
                    phase_id: "null",
                    plant_value_want: element_neo.plant_value_want,
                    havest_date: element_neo.havest_date,
                }

                let index = result.findIndex((e) => e.farmer_id === obj.farmer_id)

                if (index >= 0) {
                    let index_if = result.findIndex((e) => e.plant_id === obj.plant_id && e.farmer_id === obj.farmer_id)
                    if (index_if >= 0) {
                        // console.log("index_if : ",index_if)
                        db.query('UPDATE neofirm_plan SET plant_value_want=?, havest_date=? WHERE farmer_id=? AND plant_id=? ', [obj.plant_value_want, obj.havest_date, obj.farmer_id, obj.plant_id], (err, result) => {
                            if (err) throw err
                            else {
                                next()
                            }
                        })
                    }
                    else {
                        // console.log("index_else : ",index_if)
                        db.query('INSERT INTO neofirm_plan SET ?', obj, (err) => {
                            if (err) throw err
                            else {
                                next()
                            }
                        })
                    }
                }
                else {
                    // console.log("else : ",index)
                    db.query('INSERT INTO neofirm_plan SET ?', obj, (err) => {
                        if (err) throw err
                        else {
                            next()
                        }
                    })
                }
            })
        })
    }
}

exports.get_data_plan = () => {

    return (req, res, next) => {
        db.query(`SELECT neofirm_plan.idfamer_phase,neofirm_plan.farmer_id,neofirm_plan.plant_id,neofirm_plan.havest_date,neofirm_plan.plant_value_want,
                plant_information.plant_name,
                farmer_information.first_name,farmer_information.last_name

            FROM neofirm_plan

            INNER JOIN plant_information
            ON neofirm_plan.plant_id = plant_information.plant_id

            INNER JOIN farmer_information
            ON neofirm_plan.farmer_id = farmer_information.farmer_id
            WHERE neofirm_plan.plant_id = ? AND farmer_information.user_id = ?`, [req.params.id, req.user_id], (err, result) => {


            if (err) throw err
            else {
                let data = []
                //         result.map((element_result) => {
                //             let phase_array = JSON.parse(element_result.phase)
                //             let data_phase = null
                //             phase_array.map((phase_array_element) => {
                //                 // console.log(phase_array_element)
                //                 if (phase_array_element.id === element_result.phase_id) {
                //                     data_phase = phase_array_element
                //                     // console.log(data_phase)
                //                 }
                //             })

                ///////////////////////////
                result.map((element) => {
                    data.push({
                        idfamer_phase: element.idfamer_phase,
                        farmer_id: element.farmer_id,
                        plant_id: element.plant_id,
                        havest_date: moment(element.havest_date).format("YYYY-MM-DD"),
                        plant_value_want: element.plant_value_want,
                        plant_name: element.plant_name,
                        first_name: element.first_name,
                        last_name: element.last_name
                    })
                })
                ///////////////////////////


                //             data.push({
                //                 idfamer_phase: element_result.idfamer_phase,
                //                 first_name: element_result.first_name,
                //                 last_name: element_result.last_name,
                //                 farmer_id: element_result.farmer_id,
                //                 plant_id: element_result.plant_id,
                //                 // plant_value: element_result.plant_value != null ? element_result.plant_value : 0,
                //                 plant_name: element_result.plant_name,
                //                 plant_value_want: element_result.plant_value_want,
                //                 havest_date: moment(element_result.havest_date).format("YYYY-MM-DD"),
                //                 // phase_id: element_result.phase_id,
                //                 // phase_name: data_phase.phase_name,
                //             })


                //         })
                // console.log(data)
                req.result = data
                next()
            }
        })
    }
}

exports.update_plan = () => {
    return (req, res, next) => {

        let obj = {
            plant_value_want: req.body.plant_value_want,
            havest_date: moment(req.body.havest_date, "DD-MM-YYYY").format("YYYY-MM-DD")
        }

        db.query('UPDATE neofirm_plan SET plant_value_want=?, havest_date=? WHERE idfamer_phase=? ', [obj.plant_value_want, obj.havest_date, req.body.idfamer_phase], (err, result) => {
            if (err) throw err
            else {
                next()
            }
        })
    }
}

exports.delete_plan = () => {
    return (req, res, next) => {

        db.query('DELETE FROM neofirm_plan WHERE idfamer_phase=? ', req.body.idfamer_phase, (err) => {
            if (err) throw err
            else {
                next()
            }
        })
    }
}

exports.get_phase_name = () => {
    return (req, res, next) => {

        db.query('SELECT * FROM plant_information ', (err, result) => {
            if (err) throw err
            else {
                let data_phase = []

                result.map((result_array) => {
                    let phase_array = JSON.parse(result_array.phase)
                    let data_phase_map = []

                    phase_array.map((phase_array_element) => {
                        data_phase_map.push({
                            phase_name: phase_array_element.phase_name,
                            phase_id: phase_array_element.id
                        })
                    })

                    data_phase.push({
                        plant_id: result_array.plant_id,
                        phase: data_phase_map
                    })
                })

                req.result = data_phase
                next()
            }
        })

    }
}

////////////////////////////////////////////////////////////////////

exports.get_user_order = () => {
    return (req, res, next) => {

        db.query('SELECT * FROM neofirm_order WHERE user_id = ? AND plant_id = ? ', [req.user_id, req.params.plant_id], (err, result) => {
            if (err) throw err
            else {
                let data = []
                let order_value = null
                result.map((e) => {

                    order_value = e.order_value
                })


                data.push({
                    user_id: req.user_id,
                    plant_id: parseInt(req.params.plant_id),
                    order_value: order_value != null ? order_value : 0
                })

                req.result = data
                next()
            }
        })
    }
}

exports.set_user_order = () => {
    return (req, res, next) => {
        db.query('SELECT * FROM neofirm_order WHERE user_id = ?', req.user_id, (err, result) => {
            if (err) throw err
            else {

                let index = result.findIndex((e) => e.plant_id === req.body.plant_id)
                if (index >= 0) {
                    db.query('UPDATE neofirm_order SET order_value=? WHERE plant_id=? ', [req.body.order_value, req.body.plant_id], (err, result) => {
                        if (err) throw err
                        else {
                            next()
                        }
                    })
                }
                else {
                    db.query('INSERT INTO neofirm_order SET ?', req.body, (err) => {
                        if (err) throw err
                        else {
                            next()
                        }
                    })
                }
            }
        })
    }
}

exports.get_data_farmer_plan = () => {
    return (req, res, next) => {
        // console.log(req.params.plant_id)
        db.query(`SELECT  farmer_information.farmer_id, farmer_information.user_id, 
                farmer_information.title_name, farmer_information.first_name, 
                farmer_information.last_name, neofirm_plan.plant_id, 
                neofirm_plan.plant_value_want, neofirm_plan.havest_date

                FROM farmer_information 
                LEFT JOIN(SELECT * FROM neofirm_plan WHERE plant_id = ?) neofirm_plan
                ON farmer_information.farmer_id = neofirm_plan.farmer_id
                WHERE user_id = ?`, [req.params.plant_id, req.user_id], (err, result) => {
            if (err) throw err
            else {
                let data_farmer = []
                result.map((element) => {
                    data_farmer.push({
                        farmer_id: element.farmer_id,
                        user_id: element.user_id,
                        title_name: element.title_name,
                        first_name: element.first_name,
                        last_name: element.last_name,
                        plant_id: element.plant_id != null ? element.plant_id : req.params.plant_id,
                        plant_value_want: element.plant_value_want != null ? element.plant_value_want : 0,
                        havest_date: element.havest_date != null ? moment(element.havest_date).format("YYYY-MM-DD") : moment().format("YYYY-MM-DD")
                    })
                })
                req.result = data_farmer
                next()
            }
        })
    }
}

exports.get_plant_id_form_order = () => {
    return (req, res, next) => {
        db.query('SELECT DISTINCT plant_id FROM neofirm_plan', (err, result) => {
            if (err) throw err
            else {
                let data_plant_id = []
                result.map((e) => {
                    data_plant_id.push({
                        plant_id: e.plant_id
                    })
                })
                req.result = data_plant_id
                next()
            }
        })
    }
}

exports.get_data_product = () => {
    return (req, res, next) => {

        db.query(`SELECT * FROM plant_information 
                LEFT JOIN plant_stock ON plant_information.plant_id = plant_stock.plant_id
                WHERE  plant_information.plant_id = ?`, req.params.plant_id, (err, PlantinformationResult) => {
            if (err) throw err
            else {
                let price = null
                PlantinformationResult.map((e) => {
                    price = e.price
                })

                if (PlantinformationResult[0]) {
                    let phase = JSON.parse(PlantinformationResult[0].phase)
                    let phase_and_neo_firm = []
                    let group_phase = []
                    let graph_data = []
                    let se_data = []

                    db.query(`SELECT 
                            user_information.name as name,
                            user_information.user_id as user_id,
                            famer_phase.phase_id as phase_id,
                            famer_phase.plant_value as plant_value ,
                            famer_phase.havest_date as havest_date
                            FROM famer_phase 
                            LEFT JOIN  farmer_information ON famer_phase.farmer_id = farmer_information.farmer_id  
                            LEFT JOIN  user_information ON farmer_information.user_id = user_information.user_id 
                               
                            WHERE  famer_phase.plant_id = ? AND farmer_information.user_id = ?`, [req.params.plant_id, req.user_id], (err, result) => {
                        if (err) throw err
                        else {


                            group_phase = _.groupBy(result, (e) => e.phase_id)

                            phase.map((phase_element) => {

                                phase_element.phase = group_phase[phase_element.id] || []

                                let data_month = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                                let data_year = []
                                let group_neo_firm = []
                                let group_neo_firm_id = []
                                let neo_firm_data = []

                                let value_phase = []

                                phase_element.phase.map((group_phase_el) => {
                                    // console.log("group_phase_el : ",group_phase_el)
                                    let number_of_month = parseInt(moment(group_phase_el.havest_date).format("M"))
                                    let year_month = moment(group_phase_el.havest_date).format("YYYY-MM")

                                    // console.log(moment(number_of_year).format("M"))

                                    // if(parseInt(moment(year_month).format("M"))==number_of_month){
                                    //     data_month[number_of_month-1] += group_phase_el.plant_value
                                    // }

                                    data_month[number_of_month-1] += group_phase_el.plant_value
                                    
                                    // data_year.push({
                                    //     year: parseInt(moment(year_month).format("Y")),
                                    //     data_month: data_month
                                    // })
                                    
                                    value_phase.push({
                                        user_id: group_phase_el.user_id,
                                        name: group_phase_el.name,
                                        plant_value: group_phase_el.plant_value,
                                    })
                                })

                                // console.log("data_year :",data_year)

                                group_neo_firm = _.groupBy(value_phase, (e) => e.user_id)
                                group_neo_firm_id = _.map(phase_element.phase, (e) => e.user_id.toString())

                                group_neo_firm_id.map((group_neo_firm_id_el) => {
                                    neo_firm_data.push(...group_neo_firm[group_neo_firm_id_el])
                                })

                                // console.log(group_neo_firm_id)

                                se_data.push({
                                    phase_name: phase_element.phase_name,
                                    data_value: neo_firm_data
                                })


                                graph_data.push({
                                    name: phase_element.phase_name,
                                    data: data_month,
                                    stack: phase_element.phase_name
                                })

                                phase_and_neo_firm.push({
                                    ...phase_element,
                                })

                            })

                            req.result = {
                                series: graph_data,
                                se_data: se_data,// _.groupBy(se_data,(e)=>e.phase_name)
                                price: JSON.parse(price)
                            }

                            // console.log(req.result)
                            next()
                        }
                    })


                }
                else {
                    res.status(200).json(errorMessages.invalid_data)
                }
            }

        })
    }
}

exports.get_phase = () => {
    
    return (req, res, next) => {
        db.query('SELECT * FROM famer_phase', (err, result) => {
            if (err) throw err
            else {
                req.result = result
                next()
            }
        })
    }
}