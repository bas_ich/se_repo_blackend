var express = require('express')
var router = express.Router()
var neoFirmUtil = require('../controller/neo_firm_colltroller')
var validateUtil = require('../controller/validate_controller')

router.get('/get_order_se',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_order_se(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)
router.post('/get_detail_order_se',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_detail_order_se(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)
router.get('/get_plant_in_network',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_linechart_some_se(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.get('/get_farmer_se',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_farmer_se(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.post('/add_order_farmer',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.add_order_farmer(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            message: "สั่งวัตถุดิบเกษตรกรสำเร็จ"
        })
    }
)

router.post('/get_order_farmer',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_order_farmer(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.post('/add_invoice_se',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.add_invoice_se(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.post('/update_order_se_status',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.update_order_se_status(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            message: "ออกใบเสร็จสำเร็จ"
        })
    }
)

router.get('/get_Certified',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_Certified(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    })



router.get('/get_planing_farmer',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_planing_farmer(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    })

router.get('/get_planing_se_personal',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_planing_se_personal(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    })

router.post('/add_planing_farmer',
    validateUtil.validate_token_user(),
    neoFirmUtil.add_planing_farmer(),
    (req, res) => {
        res.status(200).json({
            success: true,
            message: "วางแผนการเพาะปลูกสำเร็จ"
        })
    }
)

router.get('/get_count_farmer',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_count_farmer(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)


router.get('/get_trading_statistics_farmer',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_trading_statistics_farmer(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)


router.get('/get_summary_personal',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_summary_personal(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)


router.get('/up_stock_se',
    // validateUtil.validate_token_se_small(),
    neoFirmUtil.up_stock_se(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)



router.get('/get_farmer',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_farmer(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.get('/get_plant_phase',
    // validateUtil.validate_token_user(),
    neoFirmUtil.get_plant_phase(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.get('/get_farmer_phase/:id',
    validateUtil.validate_token_user(),
    neoFirmUtil.get_farmer_phase(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    })
/////////////////////////////////////////////
router.post('/set_farmer_phase',
    neoFirmUtil.set_farmer_phase(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.post('/delete_farmer_phase',
    neoFirmUtil.delete_farmer_phase(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.post('/update_farmer_phase',
    neoFirmUtil.update_farmer_phase(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.get('/get_plant_data',
    neoFirmUtil.get_plant_data(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.get('/get_data_farmer_phase/:phase_id',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_data_farmer_phase(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.post('/set_data_plan',
    neoFirmUtil.set_data_plan(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.get('/get_data_plan/:id',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_data_plan(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.post('/update_plan',
    neoFirmUtil.update_plan(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.post('/delete_plan',
    neoFirmUtil.delete_plan(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.get('/get_phase_name',
    neoFirmUtil.get_phase_name(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.get('/get_user_order/:plant_id',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_user_order(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.post('/set_user_order',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.set_user_order(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.get('/get_data_farmer_plan/:plant_id',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_data_farmer_plan(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.get('/get_plant_id_form_order',
    neoFirmUtil.get_plant_id_form_order(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)

router.get('/get_data_product/:plant_id',
    validateUtil.validate_token_se_small(),
    neoFirmUtil.get_data_product(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)


router.get('/get_phase',
    neoFirmUtil.get_phase(),
    (req, res) => {
        res.status(200).json({
            'success': true,
            result: req.result
        })
    }
)


module.exports = router